USE [ipapi]
GO
/****** Object:  Table [dbo].[tblLog]    Script Date: 11/06/2013 16:30:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblLog](
	[fldUUID] [nvarchar](50) NOT NULL,
	[fldDATETIME] [datetime] NOT NULL,
	[fldAUTHUSER] [nvarchar](50) NOT NULL,
	[fldHOST] [nvarchar](125) NOT NULL,
	[fldREFERER] [nvarchar](125) NOT NULL,
	[fldUSERAGENT] [nvarchar](50) NOT NULL,
	[fldPATHINFO] [nvarchar](255) NOT NULL,
	[fldQUERYSTRING] [nvarchar](125) NOT NULL,
	[fldREMOTEADDR] [nvarchar](15) NOT NULL,
	[fldREMOTEHOST] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_tblLog] PRIMARY KEY CLUSTERED 
(
	[fldUUID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
