<!---
Incept Date: 11/06/2013
Date Last Modified: 11/06/2013
Originator: Sid Wing
Last Author: Sid Wing
Object/Page name: index.cfm
Purpose: test page for use in API development
--->
<cfinvoke component="com.statsapi" method="cgiTrimmer" returnvariable="theCGI">
	<cfinvokeargument name="theCGI" value="#CGI#" />
</cfinvoke>
<cfinvoke component="com.statsapi" method="logToDB" returnvariable="findMe">
	<cfinvokeargument name="theCGI" value="#theCGI#" />
</cfinvoke>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<body>
<table border="1" cellpadding="5" cellspacing="0">
<cfoutput>
	<tr><th>AUTH_USER</th><td>#findme.fldauthuser#</td></tr>
	<tr><th>HTTP_HOST</th><td>#findme.fldHOST#</td></tr>
	<tr><th>HTTP_REFERER</th><td>#findme.fldREFERER#</td></tr>
	<tr><th>HTTP_USER_AGENT</th><td>#findme.fldUSERAGENT#</td></tr>
	<tr><th>PATH_INFO</th><td>#findme.fldPATHINFO#</td></tr>
	<tr><th>QUERY_STRING</th><td>#findme.fldQUERYSTRING#</td></tr>
	<tr><th>REMOTE_ADDR</th><td>#findme.fldREMOTEADDR#</td></tr>
	<tr><th>REMOTE_HOST</th><td>#findme.fldREMOTEHOST#</td></tr>
</cfoutput>
</table>
</body>
</html>