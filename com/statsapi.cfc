<!---
Incept Date: 11/06/2013
Date Last Modified: 11/06/2013
Originator: Sid Wing
Last Author: Sid Wing
Object/Page name: statsapi.cfc
Purpose: collect user/browser/ip stats
--->
<cfcomponent name="statsapi" displayname="Stats API" hint="I collect/manage data from the CGI scope.">
	<cfset theDSN = "IPAPI" /> <!--- the ColdFusion DSN to use for this component - change to suit your DSN --->
    <cfset theFILE = "C:\path\to\my\logfile\log.txt" /> <!--- If you choose to log to a file - the full path the the txt file to log to --->
	<!---
	Logging to file is NOT - I repeat - NOT recommended for heavily trafficked web sites 
	--->
    
    
	<!--- Init Function - for use with Application.cfc --->
    <cffunction name="init" access="public" returntype="statsapi" output="no" hint="I instantiate and return this object.">
        <cfreturn this>
    </cffunction>

	<!--- Trim down the CGI Environment variable to a useful subset --->    
    <cffunction name="cgiTrimmer" access="public" returntype="struct" hint="Trim the CGI Environment variable to just what we need.">
    	<cfargument name="theCGI" displayname="theCGI" hint="the CGI Environment variable sent from a page" required="yes">
        <cfset var myCGI = StructNew()>
        <cfif theCGI.auth_user is not "">
	        <cfset myCGI.auth_user = theCGI.auth_user />
        <cfelse>
	        <cfset myCGI.auth_user = "UNKNOWN" />
        </cfif>
        <cfset myCGI.http_host = theCGI.http_host />
        <cfif theCGI.http_referer is not "">
	        <cfset myCGI.http_referer = theCGI.http_referer />
        <cfelse>
	        <cfset myCGI.http_referer = "NONE" />
        </cfif>
		<cfif theCGI.http_user_agent CONTAINS "MSIE">
	        <cfset myCGI.http_user_agent = "MSIE" />
		<cfelseif theCGI.http_user_agent CONTAINS "Chrome">
        	<cfset myCGI.http_user_agent = "CHROME" />
        <cfelseif theCGI.http_user_agent CONTAINS "FireFox">
        	<cfset myCGI.http_user_agent = "FIREFOX" />
        <cfelse>
        	<cfset myCGI.http_user_agent = "OTHER" />
        </cfif>
        <cfset myCGI.path_info = theCGI.path_info />
        <!--- <cfset myCGI.path_translated = theCGI.path_translated /> --- NOT USED AT THIS TIME --->
        <cfif theCGI.query_string is not "">
		    <cfset myCGI.query_string = theCGI.query_string />
        <cfelse>
            <cfset myCGI.query_string = "NONE" />
        </cfif>
        <cfif theCGI.remote_addr is "::1">
        	<cfset myCGI.remote_addr = "127.0.0.1" />
        <cfelse>
	        <cfset myCGI.remote_addr = theCGI.remote_addr />
        </cfif>
        <cfif theCGI.REMOTE_HOST is not theCGI.REMOTE_ADDR>
		    <cfset myCGI.remote_host = theCGI.remote_host />
    	<cfelse>
            <cfset myCGI.remote_host = "UNKNOWN" />
        </cfif> 
        <cfreturn myCGI />
    </cffunction>

	<!--- Log Access to the DB --->    
    <cffunction name="logToDB" access="public" returntype="query" hint="Add entry to the DB">
    	<cfargument name="theCGI" displayname="theCGI" hint="Our Trimmed CGI Structure" required="yes">
        <cfset var theUUID = CreateUUID()>
        <cfquery name="addMe" datasource="#theDSN#">
        	INSERT INTO tblLog (fldUUID, fldAUTHUSER, fldDATETIME, fldHOST, fldPATHINFO, fldQUERYSTRING, fldREFERER, fldREMOTEADDR, fldREMOTEHOST, fldUSERAGENT)
			VALUES (
            '#theUUID#',
            '#theCGI.auth_user#',
            #CreateODBCDateTime(Now())#,
            '#theCGI.http_host#',
            '#theCGI.path_info#',            
            '#theCGI.query_string#',
            '#theCGI.http_referer#',
            '#theCGI.remote_addr#',
            '#theCGI.remote_host#',
            '#theCGI.http_user_agent#')
        </cfquery>
        <cfquery name="findMe" datasource="#theDSN#">
        	SELECT *
            FROM tblLog
            WHERE fldUUID = '#theUUID#'
        </cfquery>
        <cfreturn findMe />
    </cffunction>
</cfcomponent>